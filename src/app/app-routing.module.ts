import { NgModule, ModuleWithComponentFactories } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [{
  path: 'landing',
  loadChildren: () => import('./components/landing/landing.module')
  .then(modulo => modulo.LandingModule)
},
{
  path: '',
  redirectTo: 'landing',
  pathMatch: 'full'
},
{
  path: 'clientes',
  loadChildren: () => import('./components/clientes/clientes.module')
  .then(modulo => modulo.ClientesModule)
},
{
  path: 'productos',
  loadChildren: () => import('./components/productos/productos.module')
  .then(modulo => modulo.ProductosModule)
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
