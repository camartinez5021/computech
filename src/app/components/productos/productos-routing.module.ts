import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductosCreateComponent } from './productos-create/productos-create.component';
import { ProductosListComponent } from './productos-list/productos-list.component';
import { ProductosUpdateComponent } from './productos-update/productos-update.component';
import { ProductosViewComponent } from './productos-view/productos-view.component';


const routes: Routes = [{
  path: 'productos-create',
  component: ProductosCreateComponent
},
{
  path: 'productos-list', 
  component: ProductosListComponent
},
{
  path: 'productos-update', 
  component: ProductosUpdateComponent
},
{
  path: 'productos-view',
  component: ProductosViewComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductosRoutingModule { }
