import { Component, OnInit } from '@angular/core';
import { Productos } from '../Interface/productos';
import { ProductosService } from '../productos.service';

@Component({
  selector: 'app-productos-list',
  templateUrl: './productos-list.component.html',
  styleUrls: ['./productos-list.component.styl']
})
export class ProductosListComponent implements OnInit {

  public clientesList: Productos[];

  constructor(private productosService: ProductosService) { }


  ngOnInit() {
  this.productosService.query()
  .subscribe(res => {
    this.clientesList = res;
    console.log('Response data ddd', this.clientesList)
  },
  error => console.error('Error ', error))
};

}
