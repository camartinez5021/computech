import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { HeaderComponent } from './header/header.component';
import { DestacadosComponent } from './destacados/destacados.component';
import { SlideComponent } from './slide/slide.component';
import { ContactoComponent } from './contacto/contacto.component';
import { FooterComponent } from './footer/footer.component';
import { LandingRoutingModule } from './landing-routing.module';



@NgModule({
  declarations: [MainComponent, HeaderComponent, DestacadosComponent, SlideComponent, ContactoComponent, FooterComponent],
  imports: [
    CommonModule, LandingRoutingModule
  ]
})
export class LandingModule { }
