import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientesCreateComponent } from './clientes-create/clientes-create.component';
import { ClientesListComponent } from './clientes-list/clientes-list.component';
import { ClientesUpdateComponent } from './clientes-update/clientes-update.component';
import { ClientesViewComponent } from './clientes-view/clientes-view.component';
import { ClientesRoutingModule } from './clientes-routing.module';



@NgModule({
  declarations: [ClientesCreateComponent, ClientesListComponent, ClientesUpdateComponent, ClientesViewComponent],
  imports: [
    CommonModule, ClientesRoutingModule
  ]
})
export class ClientesModule { }
