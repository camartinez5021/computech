export interface Clientes {
    id: number;
    client_name: string;
    client_email: string;
    phone_number: string;
    document_number: string;
}