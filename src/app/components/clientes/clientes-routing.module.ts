import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientesCreateComponent } from './clientes-create/clientes-create.component';
import { ClientesListComponent } from './clientes-list/clientes-list.component';
import { ClientesUpdateComponent } from './clientes-update/clientes-update.component';
import { ClientesViewComponent } from './clientes-view/clientes-view.component';


const routes: Routes = [{
  path: 'clientes-create',
  component: ClientesCreateComponent
},
{
  path: 'clientes-list', 
  component: ClientesListComponent
},
{
  path: 'clientes-update', 
  component: ClientesUpdateComponent    
},
{
  path: 'clientes-view',
  component: ClientesViewComponent
},];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientesRoutingModule { }
