import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Clientes } from './Interface/clientes';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  constructor(private http: HttpClient){}

  public query(): Observable<Clientes[]>{
    return this.http.get<Clientes[]>(`${environment.END_POINT}/api/client`)
    .pipe(map(res => {return res;
    }))
  }
}