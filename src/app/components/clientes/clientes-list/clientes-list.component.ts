import { Component, OnInit } from '@angular/core';
import { Clientes } from '../Interface/clientes';
import { ClientesService } from '../clientes.service';

@Component({
  selector: 'app-clientes-list',
  templateUrl: './clientes-list.component.html',
  styleUrls: ['./clientes-list.component.styl']
})
export class ClientesListComponent implements OnInit {

  public clientesList: Clientes[];

  constructor(private clientesService: ClientesService) { }


  ngOnInit() {
  this.clientesService.query()
  .subscribe(res => {
    this.clientesList = res;
    console.log('Response data ddd', this.clientesList)
  },
  error => console.error('Error ', error))
};


}
